import './App.css';
import React, { useState, useRef, useEffect } from "react"
import { BrowserRouter as Router, Route, Switch, } from "react-router-dom"
import Geozone from './Component/Geozone'
import Create from './Component/Create';

function App() {
  const [geozones, setGeozones] = useState([])
  const [geozoneList, setGeozoneList] = useState([
    {
      "name": "new",
      "id": 1631014878426,
      "coords": [
          [
              41.311539292689524,
              69.27287054492186
          ],
          [
              41.30118402426299,
              69.28145361376949
          ],
          [
              41.29859494824953,
              69.2529578251953
          ],
          [
              41.30765626121208,
              69.2481513066406
          ],
          [
              41.311539292689524,
              69.27287054492186
          ]
      ]
  }
  ])
  const [status, setStatus] = useState(0)

  useEffect(() => {
    let list = []
    geozoneList?.map((val, i) => {list.push([]); val?.coords?.map((el, j) => { list[i].push(el) }) })
    setGeozones(list)
  }, [geozoneList])

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <Geozone geozoneList={geozoneList} geozones={geozones} setStatus={setStatus} status={status} />
          </Route>
          <Route path="/create">
            <Create geozoneList={geozoneList} setGeozoneList={setGeozoneList} setStatus={setStatus} status={status} />
          </Route>
        </Switch>
      </Router>

    </div>
  );
}

export default App;
