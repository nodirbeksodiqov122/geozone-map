import React, { useState, useRef, useEffect } from "react";
import { YMaps, Map, Polygon } from "react-yandex-maps";
import Form from "./Form";
import { useHistory, useLocation } from "react-router-dom";

const defaultMapState = {
  center: [41.311151, 69.279737],
  zoom: 12,
  controls: [
    "zoomControl",
    "fullscreenControl",
    "geolocationControl",
    "rulerControl",
    "trafficControl",
    "typeSelector",
  ],
};

export default function Create({
  geozoneList,
  setStatus,
  status,
  setGeozoneList,
}) {
  const draw = (ref) => {
    ref.editor.startDrawing();

    // ref.editor.events.add("statechange", event => {
    //   console.log(event);
    // });
    ref.geometry.events.add("change", (e) =>
      setnewCoords(e.get("newCoordinates"))
    );
  };
  const [newCoords, setnewCoords] = useState([]);
  const [geozone, setGeozone] = useState([]);
  const state = useLocation()?.state;
  const ymapRef = useRef(null);
  useEffect(() => {
    setGeozone([state?.coords || []]);
    setnewCoords([state?.coords || []])
  }, []);
  return (
    <div
      style={{
        backgroundColor: "grey",
        width: "100%",
        height: "100vh",
      }}
    >
      <Form
        state={state}
        geozoneList={geozoneList}
        setStatus={setStatus}
        status={status}
        setGeozoneList={setGeozoneList}
        newCoords={newCoords}
      />
      <YMaps
        ref={ymapRef}
        query={{
          lang: "ru_RU",
          load: "package.full",
          apikey: "679a08be-aa49-4a79-ad31-80c65dda374a",
        }}
      >
        <Map
          width="100%"
          height="100%"
          // state={mapState}
          defaultState={defaultMapState}
        >
          <Polygon
            instanceRef={(ref) => ref && draw(ref)}
            geometry={geozone}
            // defaultState={geozones}

            options={{
              editorDrawingCursor: "crosshair",
              //   editorMaxPoints: 5,

              fillColor: "#00FF00",

              opacity: 0.5,
              // Цвет обводки.
              strokeColor: "#0000FF",
              // Ширина обводки.
              strokeWidth: 5,
            }}
          />
        </Map>
      </YMaps>
    </div>
  );
}
