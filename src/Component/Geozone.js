import React, { useState, useRef, useEffect } from "react"
import { YMaps, Map, Polygon } from "react-yandex-maps"
import Form from "./Form";

let defaultMapState = {
    center: [41.311151, 69.279737],
    zoom: 12,
    controls: [
        "zoomControl",
        "fullscreenControl",
        "geolocationControl",
        "rulerControl",
        "trafficControl",
        "typeSelector"
    ]
}

// const draw = ref => {
//     ref.editor.startDrawing();

//     // ref.editor.events.add("statechange", event => {
//     //   console.log(event);
//     // });
//     ref.geometry.events.add('change', (e) => console.log(e.get('newCoordinates')));
// };

export default function Create({geozoneList,geozones,setStatus, status}) {
    
    const ymapRef = useRef(null)

    return (
        <div
            style={{
                backgroundColor: "grey",
                width: "100%",
                height: "100vh",
            }}
        >
            <Form geozoneList={geozoneList} setStatus={setStatus} status={status}/>
            <YMaps
                ref={ymapRef}
                query={{
                    lang: "ru_RU",
                    load: "package.full",
                    apikey: "679a08be-aa49-4a79-ad31-80c65dda374a",
                }}
            >
                <Map
                    width="100%"
                    height="100%"
                    // state={geozones}
                    defaultState={defaultMapState}
                >
                    <Polygon
                        // instanceRef={ref => ref && draw(ref)}
                        geometry={geozones}
                        options={{
                            editorDrawingCursor: "crosshair",
                            //   editorMaxPoints: 5,
                            fillColor: "#00FF00",
                            opacity: 0.5,
                            strokeColor: "#0000FF",
                            strokeWidth: 5
                        }}
                    />
                </Map>
            </YMaps>

        </div>
    )
}
