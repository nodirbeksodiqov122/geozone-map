import React, { useState } from "react";
import { useHistory } from "react-router";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { IconButton } from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import SubjectIcon from "@material-ui/icons/Subject";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ClearAllIcon from "@material-ui/icons/ClearAll";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import TextField from "@material-ui/core/TextField";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import Button from "@material-ui/core/Button";
import "./style.css";
import DeleteOutlined from "@material-ui/icons/DeleteOutlined";

export default function Form({
  state,
  geozoneList,
  setStatus,
  status,
  setGeozoneList,
  newCoords,
}) {
  const history = useHistory();
  const [checked, setChecked] = useState(false);
  const [title, setTitle] = useState(state?.name || "");
  const [titleError, setTitleError] = useState(false);

  const handleItem = (path, val) => {
    if (path === "/create") {
      setStatus(1);
      history.push({ pathname: "/create", state: val });
    } else {
      setStatus(0);
      history.push({ pathname: "/" });
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setTitleError(false);

    if (title === "") {
      setTitleError(true);
    }

    let data = {
      name: title,
      id: Date.now(),
      coords: newCoords[0],
    };
    console.log(data);

    if (title && !state) {
      setGeozoneList(oldArray => [...oldArray, data])
      history.push('/')
      setStatus(false)
    }

    if (title && state) {
      setGeozoneList(old => old.map((val)=>state.id===val.id?{...old,name:title,coords:newCoords[0]}:null))
      history.push('/')
      setStatus(false)
    }
  };

  const handleDel = (id) => {
    // setGeozoneList(old => old.map((val)=>state.id!==val.id&&{...val}))
    // history.push('/')
    // setStatus(false)
  }

  return (
    <div className="collapse-geozone">
      <IconButton
        variant="outlined"
        size="large"
        onClick={() => {
          setChecked((prev) => !prev);
        }}
      >
        <SubjectIcon />
      </IconButton>
      <Drawer
        sx={{
          width: 240,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: 240,
            boxSizing: "border-box",
          },
        }}
        variant="persistent"
        anchor="left"
        open={checked}
      >
        <List>
          <ListItem
            onClick={() => {
              setChecked((prev) => !prev);
            }}
            button
          >
            <ListItemIcon>
              <ArrowBackIosIcon />
            </ListItemIcon>
          </ListItem>
          <ListItem onClick={() => handleItem("/create")} button>
            <ListItemIcon>
              <AddCircleOutlineIcon />
            </ListItemIcon>
            <ListItemText primary="Create" />
          </ListItem>
          <ListItem onClick={() => handleItem("/")} button>
            <ListItemIcon>
              <ClearAllIcon />
            </ListItemIcon>
            <ListItemText primary="All geozones" />
          </ListItem>
        </List>
        <Divider />
        {status === 1 ? (
          <form noValidate autoComplete="off" onSubmit={handleSubmit}>
            <List>
              <ListItem>
                <TextField
                  onChange={(e) => setTitle(e.target.value)}
                  defaultValue={title}
                  label="Name"
                  variant="outlined"
                  fullWidth
                  required
                  error={titleError}
                />
              </ListItem>
              <ListItem>
                <Button
                  type="submit"
                  variant="contained"
                  endIcon={<KeyboardArrowRightIcon />}
                >
                  Submit
                </Button>
              </ListItem>
            </List>
          </form>
        ) : (
          <List>
            {geozoneList?.map((val, i) => (
              <ListItem
                key={i}
                button
                
              >
                <ListItemText onClick={() => handleItem("/create", val)} primary={val?.name} />
                <ListItemIcon onClick={() => handleDel(val.id)}><DeleteOutlined/></ListItemIcon>
              </ListItem>
            ))}
          </List>
        )}
      </Drawer>
    </div>
  );
}
